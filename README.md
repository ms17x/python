# LIS4369 Extensible Interprise Solutions

## Mitchnikov Seide

### LIS4369 Requirements:

*Course Work Links:*

[A1 README.md](a1/README.md "My A1 README.md file")

- Install Python
- Install R
- Install R Studio
- Install Visual Studio Code
- Create a1_tip_calculator application
- Provide screenshots of installations
- Create Bitbucket repo
- Complete Butbucket tutorial
- Provide git command descriptions

[A2 README.md](a2/README.md "My A2 README.md file")

- Backward-engineer (using Python) the following screenshots:
- The programshould be organized with two modules(See Ch. 4): 
- functions.pymodulecontainsthefollowing functions:i)get_requirements()ii)calculate_payroll()iii)print_pay
- main.pymodule imports the functions.pymodule, and callsthe functions. 
- Be sure to test your program using both IDLEand Visual Studio Code.
	
[A3 README.md](a3/README.md "My A3 README.md file")

- Backward-engineer (using Python) the following screenshots:
- The program should be organized with two modules (See Ch. 4):
- functions.pymodulecontains the following functions:i.get_requirements()ii.estimate_painting_cost()iii.print_painting_estimate()iv.print_painting_percentage()
- main.pymodule imports the functions.pymodule, and callsthe functions. 
- Be sure to test your program using both IDLEand Visual Studio Code.


[A4 README.md](a4/README.md "My A4 README.md file")

- If erros, more than likely missing installations
- Test Python Package Installer: pip freeze
- Reserach how to install any missing packages:
- Create at least three functions that are called by the program:
- main(): calls at least two other fuctions
- get_requirments(): displays the program requirements.
- data_analysis_1(): displaysthe following data.
- Display graph as per instructions w/in demo.py

[P1 README.md](p1/README.md "My P1 README.md file")

- Requirements:
- Code and run demo.py.(Note: *be sure* necessary packages are installed!)
- Then.use it tobackward-engineer the screenshotsbelow it.
- Installing Python packagespiphelper videos:

[A5 README.md](a5/README.md "My A5 README.md file")

- a.Complete the following tutorial: Introduction_to_R_Setup_and_Tutorial
- b.Code and run lis4369_a5.R(see below)
- c.Be sure to include at least two plotsin your README.md file
- Be sure to test your program using RStudio.

[P2 README.md](p2/README.md "My P2 README.md file")

- a.Use Assignment 5 screenshotsand references aboveforthe following requirements:
- b.Backward-engineer the lis4369_p2_requirements.txtfilec.
- Be sure to include at least two plotsin your README.md file.2.
- Be sure to test your program using RStudio.