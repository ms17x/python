import functions as functions

def main():
    functions.get_requirements()
    functions.estimate_painting_cost()
    while True:
        a = input("Estimate another paint job? (yes/no)")
        if a=="yes":
            functions.estimate_painting_cost()
            continue
        elif a=="no":         
            print("Thank you for using our Painting Estimator!")
            print("Please see our web site: bitbucket.org/ms17x")
            break
        

if __name__ == "__main__":
    main()
