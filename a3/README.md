# LIS4369 Extensible Enterprise Solutions

## Mitchnikov Seide


### Assignment #3 Requirements:
- 1.Backward-engineer (using Python) the following screenshots:
- 2.The program should be organized with two modules (See Ch. 4):
- a.functions.pymodulecontains the following functions:i.get_requirements()ii.estimate_painting_cost()iii.print_painting_estimate()iv.print_painting_percentage()
- b.main.pymodule imports the functions.pymodule, and callsthe functions. 
- 3)Be sure to test your program using both IDLEand Visual Studio Code.

#### Assignment Screenshots:

*Screenshot code and output*:

![assignment 1 code and output](img/a3.PNG)
*Screenshot code and output*:

![assignment 1 code and output](img/idle.PNG)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
