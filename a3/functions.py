def get_requirements():
    print("painting estimator")
    print("\nProgram Requirements:")
    print("1. calculate home interior paint cost (w/o printer).\n"
    + "2. must use float data types.\n"
    + "3. must use SQFT_PER_GALLON constant (350).\n"
    + "4. must use iteration structure (aka loop).\n"
    + "5. format, right-align numbers, and round to two decimal places. \n"
    + "6. create at least five functions that are called by the program:\n"
        + "\ta. main(): calls two other functions: get_requirements() and estimate_painting_cost().\n"
        + "\tb. get_requirements(): displays the program rewuirements.\n"
        + "\tc. estimate_painting_cost(): calculates interior home painting, and calls print functions.\n"
        + "\td. print_painting_estimate(): displays painting costs. \n"
        + "\te. print_painting_percentage(): displays painting costs percentages. \n")

def estimate_painting_cost():

    SQFT_PER_GALLON = 350

    print("\nInput: ")
    sq_ft = float(input("enter total interior sw ft: "))
    price_per_gallon = float(input("enter price per gallon point: "))
    labor_per_sqft = float(input("enter hourly painting rate per sq ft: "))

    gal = sq_ft / SQFT_PER_GALLON

    paint_cost = gal * price_per_gallon
    labor_cost = labor_per_sqft * sq_ft
    total = paint_cost + labor_cost

    paint_percent = paint_cost / total
    labor_percent = labor_cost / total
    total_percent = total / total

    print_painting_estimate(sq_ft, SQFT_PER_GALLON, gal, price_per_gallon, labor_per_sqft)
    print()
    print_painting_percentage(paint_cost, paint_percent, labor_cost, labor_percent, total, total_percent)

def print_painting_estimate(sq_ft, SQFT_PER_GALLON, gal, price_per_gallon, labor_per_sqft):

    print("\nOutput: ")
    print("{0:20} {1:>10}".format("item", "amount"))
    print("{0:20} {1:>10,.2f}".format("total sq ft:", sq_ft))
    print("{0:20} {1:>10.2f}".format("sqf ft per gallon:", SQFT_PER_GALLON))
    print("{0:20} {1:>10.2f}".format("number of gallons:", gal))
    print("{0:20} ${1:>9.2f}".format("price per gallon:", price_per_gallon))
    print("{0:20} ${1:>9.2f}".format("labor per sq ft:", labor_per_sqft))

def print_painting_percentage(paint_cost, paint_percent, labor_cost, labor_percent, total, total_percent):

    print("{0:8} {1:>9} {2:>15}".format("Cost", "Amount", "Percentage"))
    print("{0:8} {1:>8,.2f} {2:>15.2%}".format("Paint:", paint_cost, paint_percent))
    print("{0:8} {1:>8,.2f} {2:>15.2%}".format("Labor:", labor_cost, labor_percent))
    print("{0:8} {1:>8,.2f} {2:>15.2%}".format("Total:", total, total_percent))