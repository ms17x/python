# LIS4369 Extensible Enterprise Solutions

## Mitchnikov Seide


### Assignment #5 Requirements:

- Requirements:
- a.Complete the following tutorial: Introduction_to_R_Setup_and_Tutorial
- b.Code and run lis4369_a5.R(see below)
- c.Be sure to include at least two plotsin your README.md file
- Be sure to test your program using RStudio.

#### Assignment Screenshots:

*Screenshot code and output*:
![assignment 1 code and output](img/a1.PNG)

*Screenshot code and output*:
![assignment 1 code and output](img/a2.PNG)

*Screenshot code and output*:
![assignment 1 code and output](img/a3.PNG)

