import pandas as pd
import datetime
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from matplotlib import style

def get_requirments():
    print("Project 1")
    print("\nProgram requirements:\n"
        + "1. Run demo.py.\n"
        + "2. If erros, more than likely missing installations.\n"
        + "3. Test Python Package Installer: pip freeze\n"
        + "4. Reserach how to do the following intsallations:\n"
        + "     a. pandas (only if missing) \n"
        + "     b. pandas-datareader (only if missing) \n"
        + "     c. Matplotlib (only if missing) \n"
        + "5. Create at least three functions that are called by the program: \n"
        + "     a. main(): calls at least two other fuctions \n"
        + "     b. get_requirments(): displays the program requirements.\n"
        + "     c. data_analysis_1(): displaysthe following data.")

def data_analysis_1():
    start = datetime.datetime(2010,1,1)
    end = datetime.datetime.today()

    df = pdr.DataReader("XOM","yahoo", start,end)

    #get user data
    print("\nPrint number of records:")
    print(len(df.index))
    
    print("\nPrint columns:")
    print(df.columns)

    print("\nPrint data frame: ")
    print(df) #note: for efficiency, only prints 60--not *all* records

    print("n\Print first five lines:")
    print(df.head(5))

    print("\nPrint last five lines:")
    print(df.tail(5))

    print("\nPrint first two lines:")
    print(df.head(2))

    print("\nPrint last two lines:")
    print(df.tail(2))

    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()