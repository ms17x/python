# LIS4369 Extensible Enterprise Solutions

## Mitchnikov Seide


### Project #1 Requirements:
- 1.Requirements:
- a.Code and run demo.py.(Note: *be sure* necessary packages are installed!)
- b.Then.use it tobackward-engineer the screenshotsbelow it.
- c.Installing Python packagespiphelper videos:
- See “Program Requirements”screenshot below.
- a.https://www.youtube.com/watch?v=U2ZN104hIcc
- b.https://www.youtube.com/watch?v=bij66_Jtoqs
- Note:*Be sure*to log in as administrator(command prompt, or sudoLINUX/Mac)
- Also, do *your* own research on Python package installing and updating.
- 2.Be sure to test your program using both IDLEand Visual Studio Code.

#### Assignment Screenshots:

*Screenshot code and output*:

![assignment 1 code and output](img/p1.PNG)
*Screenshot of a1 code and output*:

![assignment 1 isntall proof](img/installations.PNG)

*Screenshot code and output*:

![assignment 1 code and output](img/idle.PNG)