# LIS4369 Extensible Enterprise Solutions

## Mitchnikov Seide


### Assignment #1 Requirements:
- Install Python
- Install R
- Install R Studio
- Install Visual Studio Code
- Create a1_tip_calculator application
- Provide screenshots of installations
- Create Bitbucket repo
- Complete Butbucket tutorial(bitbucketstationlocations)
- Provide git command descriptions

#### Assignment Screenshots:

*Screenshot code and output*:

![assignment 1 code and output](img/tip_calc.PNG)
*Screenshot of a1 code and output*:

![assignment 1 isntall proof](img/installations.PNG)

*Screenshot code and output*:

![assignment 1 code and output](img/idle.PNG)
