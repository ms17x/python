sink("lis4369_p2.txt", append=FALSE, split=TRUE)

cat("*** Assignment Requirements ***\n")
cat("\n1. Use Assignment 5 screenshots and R Manual to backward-engineer the following requirements: ")
cat("\n2. Resources:")
cat("\n\ta. R Manual: https://cran.r-project.org/doc/manuals/r-release/R-lang.pdf")
cat("\n\tb. R for Data Science: https://r4ds.had.co.nz/ ")
cat("\n3. Use Motor Trend Car Road Tests data:")
cat("\n\ta. Research the data! https://stat.ethz.ch/R-manual/R-devel/library/datasets/html/mtcars.html")
cat("\n\tb. url = http://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv")
cat("\nNote: Use variable 'mtcars' to read file into. (See Assignment 5 for reading .csv files.) ")
cat("\n################################################### \n")

url = "http://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv"
mtcars <- read.csv(file=url,head=TRUE,sep=",")

cat("1) Display all data from file: \n")
mtcars

cat("\n 2) Display 1st 10 records: \n")
head(mtcars,10)

cat("\n 3) Display last 10 records:\n")
tail(mtcars,19)

cat("\n 4) Display file structure (see notes above):\n")
str(mtcars)

cat("\n 5) Display column names (see notes above):\n")
names(mtcars)

cat("\n 6) Display 1st record/row with column names (see notes above):\n")
mtcars[1,]

cat("\n 7) Display 2nd column data (mpg), using column number: \n")
mtcars[,2]

cat("\n 8) Display column data (cyl), using column name: \n")
mtcars$cyl

cat("\n 9) Display row/column data (3,4), that is, one field, using square bracket notation (see above):\n")
mtcars[3,4]

cat("\n 10) Display all data for cars having greater than 4 cylinders: \n")
mtcars[mtcars$cyl>4,]

cat("\n 11) Display all cars having more than 4 cylinders *and* greater than 4 gears:\n")
mtcars[mtcars$cyl>4 &mtcars$gear>4,]

cat("\n 12) Display all cars having more than 4 cylinders *and* exactly 4 gears:\n")
mtcars[mtcars$cyl>4 &mtcars$gear==4,]

cat("\n 13) Display all cars having more than 4 cylinders *or* exactly 4 gears:\n")
mtcars[mtcars$cyl>4 |mtcars$gear==4,]

cat("\n 14) Display all cars having more than 4 cylinders that do *not* have 4 gears: \n")
mtcars[mtcars$cyl>4 &mtcars$cyl!=4,]

cat("\n 15) Display total number of rows (only the number): \n")
nrow(mtcars)

cat("\n 16) Display total number of columns (only the number):\n")
ncol(mtcars)

cat("\n 17) Display total number of dimensions (i.e., rows and columns):\n")
dim(mtcars)

cat("\n 18) Display data frame structure - same as info in Python:\n")
str(mtcars)

cat("\n 19) Get mean, median, minimum, maximum, quantiles, variance, and standard deviation of horsepower:\n")
cat("\ta. Mean: \n")
mean(mtcars$hp, na.rm=TRUE)

cat("\tb. Median: \n")
median(mtcars$hp, na.rm=TRUE)

cat("\tc. Minimum: \n")
min(mtcars$hp, na.rm=TRUE)

cat("\td. Maximum: \n")
max(mtcars$hp, na.rm=TRUE)

cat("\te. Quantiles: \n")
quantile(mtcars$hp, na.rm=TRUE)

cat("\tf. Variance: \n")
var(mtcars$hp, na.rm=TRUE)

cat("\tg. Standard Deviation: \n")
sd(mtcars$hp, na.rm=TRUE)

cat("\n 20) summary() function prints min, max, mean, median, and quantiles (also, number of NA's, if any.): \n")
summary(mtcars$hp, na.rm=TRUE)

library(ggplot2)
png("plot_disp_and_mpg_1.png")
qplot(mtcars$disp, mtcars$mpg, data=mtcars,
      xlab = "Displacement",
      ylab = "MPG",
      colour = cyl,
      main="Mitchnikov Seide: Displacement vs MPG")
dev.off()

png("plot_disp_and_mpg_2.png")
plot(mtcars$wt, mtcars$mpg,
      xlab = "Weight in Thousands",
      ylab = "MPG",
      main="Mitchnikov Seide: Weight vs MPG",
      las=1)
dev.off()




