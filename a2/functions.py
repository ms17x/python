def get_requirements():
    
    print("Payroll Calculator\n")
    print("Program Requirements: \n"
        + "1. Must use float data type for user input.\n"
        + "2. Overtime rate: 1.5 times hourly rate (hours over 40).\n"
        + "3. Holiday rate: 2.0 times hourly rate (all holiday hours).\n"
        + "4. Must format currency with dollar sign, and round to two decimal places.\n"
        + "5. Create at least three functions that are called by the program\n"
        +"\ta. main(): calls at least two other functions.\n"
        +"\tb. get_requirements(): displays the program requirements.\n"
        +"\ta. calculate_payroll(): calculates an individual one-week paycheck.\n")

def calculate_payroll():
    
    hours, holiday_hours, rate = get_input()

    base_hours, overtime_hours= check_overtime(hours)
    
    base_pay = base_hours * rate
    overtime_pay = overtime_hours * (rate * 1.5)
    holiday_pay = holiday_hours * (rate * 2)

    gross_pay = base_pay + overtime_pay + holiday_pay

    print_payroll(base_pay, overtime_pay, holiday_pay, gross_pay)
    

def print_payroll(base,overtime,holiday,gross):
    
    print("\nOutput:\n"
        + "Base:     " + format_to_money(base) + "\n"
        + "Overtime: " + format_to_money(overtime) + "\n"
        + "Holiday:  " + format_to_money(holiday) + "\n"
        + "Gross:    " + format_to_money(gross))
    pass

def get_input():
    
    print("Input:")
    hours_worked = float_handler("Enter hours worked: ")
    holiday_hours = float_handler("Enter holiday hours: ")
    pay_rate = float_handler("Enter hourly pay rate: ")

    return hours_worked, holiday_hours, pay_rate

def float_handler(phrase):
    
    try:
        return float(input(phrase))
    except:
        print("!!Please enter a numerical value!!\n")
        return float_handler(phrase)

def check_overtime(hours):
    
    if(hours > 40):
        overtime_hours = hours - 40
        base_hours = 40
    else:
        base_hours = hours
        overtime_hours = 0

    return base_hours, overtime_hours
    
def format_to_money(number):
    
    decimalPlaces = "{0:,.2f}".format(number)
    money = '$%s' % decimalPlaces
    return money