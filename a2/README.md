
# LIS4369 Extensible Enterprise Solutions

## Mitchnikov Seide


### Assignment #2 Requirements:
- 1)Backward-engineer (using Python) the following screenshots:
- 2)The programshould be organized with two modules(See Ch. 4): 
- a)functions.pymodulecontainsthefollowing functions:i)get_requirements()ii)calculate_payroll()iii)print_pay
- b)main.pymodule imports the functions.pymodule, and callsthe functions. 
- 3)Be sure to test your program using both IDLEand Visual Studio Code.

#### Assignment Screenshots:

*Screenshot code and output*:

![assignment 1 code and output](img/a2.PNG)
*Screenshot code and output*:

![assignment 1 code and output](img/idle.PNG)
