print( "Program requirments:\n"
+ "1. Research: number of square feet to acres of land.\n"
+ "2. Must use float data type for user input and calulations.\n"
+ "3. Format and round conversion to two decimals places.\n")
print("\nInput: ")
miles = float(input("Enter miles driven: "))
gallon = float(input("Enter gallons of fuel used: "))

mpg = miles/gallon
mpg = round(mpg, 3)
print("\nOutput: ")
print("{:,.2f}".format(miles),"miles driven and  "  + format(gallon, ".2f" ), " gallons used = " + format(mpg, ".2f"), "mpg")