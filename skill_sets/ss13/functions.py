import math

def get_requirements():
    print("Sphere volume Program")
    print( "Program requirments:\n"
        + "\n1. Program converts user-entered temperatures into Fahrenheit or Celsius scales."
        + "\n2. Program continues to prompt for user entry until no longer requested"
        + "\n3. Note: upper and lower case letteres permitted. THough, incorrect entries are not permitted."
        + "\n4. Note: Program does not validate numeric data (optional requirement).")

def sphere_volume():
    diameter = 0
    volume = 0.0
    gallons = 0.0
    choice = ''


    print("Input:")

    choice = input(

        "DO you want to calculate a sphere volume (y or n)? ").lower()

    print("\nOutput:")

    while (choice[0] == 'y'):
        diameter = input("Please enter diameter in inches: ")
        test = True
        while (test == True):
            try:
                diameter=int(diameter)
                volume = ((4.0/3.0) * math.pi* math.pow(diameter/2.0,3))
                gallons = volume/231
                print(
                    "\nSphere volume: {0:,.2f} liquid U.S gallons\n".format(gallons))
                test = False 

            except ValueError:
                print("\nNot valid integer!")
                diameter = input("Please enter diameter in inches: ")
            continue

        choice = input(
            "Do you want to calculate another sphere volume?").lower()

    print("\nThank you for using our Sphere Vlume Calculator!")           
            