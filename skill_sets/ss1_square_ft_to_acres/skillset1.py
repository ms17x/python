print("square feet to acres \n")
print("program requirements: \n"
     + "1. research: number of square feet to acre of land.\n"
     + "2. must use float data type for user input and calculation.\n"
     + "3. format and round conversion to two decimal places.\n")

print("input:")

feet_per_acre = 43560

square_feet = float(input("enter squre feet: "))

ft_to_acre = square_feet / feet_per_acre
ft_to_acre = "{0:,.2f}".format(ft_to_acre)
square_feet = "{0:,.2f}".format(square_feet)

print("\n"+square_feet + " square feet = " + ft_to_acre + " acres")