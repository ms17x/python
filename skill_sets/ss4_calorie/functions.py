def get_requirements():
    print("calorie percentage")
    print( "Program requirments:\n"
    + "1. find calories per grams of fat, carbs, and protein.\n"
    + "2. calculate percentages.\n"
    + "3. must use float data types.\n"
    + "4. Format, right align numbers, and round to two decimal places.\n")


def calculate_calories():

        fat_grams = 0.0
        carb_grams = 0.0
        protein_grams = 0.0
        total_calories = 0.0
        percent_fat = 0.0
        percent_carbs = 0.0
        percent_protein = 0.0

        print("input:")
        fat_grams = float(input("enter total fat grams: "))
        carb_grams = float(input("enetr total carb grams: "))
        protein_grams = float(input("enter total protein grams: "))

        calories_from_fat = fat_grams * 9
        calories_from_carbs = carb_grams * 4
        calories_from_protein = protein_grams * 4
        total_calories = calories_from_fat + calories_from_carbs + calories_from_protein 

        percent_fat = calories_from_fat / total_calories
        percent_carbs = calories_from_carbs / total_calories
        percent_protein = calories_from_protein / total_calories

        print("\nOutput: ")
        print("{0:8} {1:>10} {2:>13}" .format("type","calories","percentage"))
        print("{0:8} {1:10,.2f} {2:13.2%}" .format(
            "fat", calories_from_fat, percent_fat))
        print("{0:8} {1:10,.2f} {2:13.2%}".format(
            "carbs", calories_from_carbs, percent_carbs))
        print("{0:8} {1:10,.2f} {2:13.2%}".format(
            "protein", calories_from_protein, percent_protein))