def get_requirements():
    print("Program Requirements:")
    print("1. print while loop.\n" +
          "2. Print for loops using range() function, and implicit and explicit lists.\n"+
          "3. use break and continue statements.\n" +
          "4. Replicate display below.\n")

def print_loops():

   print("1. while loop:")
   i=1
   while i <= 3:
    print(i)
    i= i + 1

   print("\n2. forr loop: using range() functon with 1 arg")
   for i in range(4):
    print(i)

   print("\n3.for loop using range() function with two args")
   for i in range(1,4):
    print(i)

   print("\n4. for loop: using range() function with three args (interval 2):")
   for i in range(1,4,2):
    print(i)

   print("\n5. for loop: using range() function with three args (interval 2):")  
   for i in range(3,0,-2):
    print(i)

   print("\n6. for loop using (implicit) list:")  
   for i in [1,2,3]:
        print(i)

   print("\n7. for loop: string list:")  
   states = ['michigan','alabama','florida']
   for state in states:
    print(state)

   print("\n8. for loop: stops loop:")  
   states = ['michigan','alabama','florida']
   for state in states:
    if state == 'florida':
        break 
    print(state)

   print("\n9. for loop: using range() (stops and continues with next):")    
   states = ['michigan','alabama','florida']
   for state in states:
    if state == 'albama':
        continue 
    print(state)

   print("\n10. print list length:")  
   states = ['michigan','alabama','florida']
   print(len(states))