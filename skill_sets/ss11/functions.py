import random

def get_requirements():

    print("pseudo-random number generator")
    print("\nProgram requirements: \n"
       +"1. get user begining and ending integer, values, and store in two variables.\n"
       +"2. Display 10 pseudo-random numbers between, and including, above values.\n"
       +"3. Must use integer data types.\n"
       +"4. Example 1: Using range() and randit() functions.\n"
       +"5. Example 2: Using a list with range() and shuffle() functions.\n")

def random_numbers():
    start = 0
    end = 0

    print("Input:")
    start = int(input("Enter begining value: "))
    end = int(input("Enetr ending value: "))

    print("\nOutput:")
    print("Example 1: Using range() and randint() functions:")
    for count in range(10):
        print(random.randint(start, end), sep=",",end=" ")

    print()

    print("Example 2: Using a list, with range() and shuffle() functions:")
    r = list(range(start, end + 1))
    random.shuffle(r)
    for i in r:
        print(i, sep=",",end="")

    print()