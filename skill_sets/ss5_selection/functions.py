def get_requirements():
    print("Program Requirements:")
    print("1. Use Python selection structure.\n" +
          "2. Prompt user for two numbers, and a suitable operator.\n"+
          "3. Test for correct numeric operator.\n" +
          "4. Replicate display below.\n")

def calculate():
    print("Python Calculator")

    num1 = float(input("Enter num1: "))
    num2 = float(input("Enter num2:"))
    x = pow(4, 2)

    print("\nSuitable Operators: +, -, *,/,//(integer division), %(modulo operator), **(power)")

    operation = input("Enter operator: ")

    if operation == "+":
        print(num1 + num2)
    elif operation == "-":
        print(num1 - num2)
    elif operation == "*":
        print(num1 * num2)
    elif operation == "/":
        print(num1 / num2)
    elif operation == "//":
        print(num1 // num2)
    elif operation == "%":
        print(num1 % num2)
    elif operation == "**":
        print(num1 ** num2) 
    elif operation == "**":
        print(pow(num1, num2))
    else:
        print("Incorrect operator!")