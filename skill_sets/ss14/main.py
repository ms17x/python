import functions as functions

def main():
    functions.get_requirements()
    functions.error_handling()

if __name__ == "__main__":
    main()