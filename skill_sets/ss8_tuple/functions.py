def get_requirements():
    print("Python Lists.\n")
    print( "Program requirments:\n"
        + "1. Lists (Python data structure): mutable, ordered sequence of elements.\n"
        + "2. List are mutable/chaneable--that is, can insert, update, delete.\n"
        + "3. Create lists - using square brackets [list]: my_lists = [cherries","apples", "Banana" , "oranges ].\n"
        + "4. Create a program that mirrors the following IPO (input/process/output) format.\n")

def using_tuples():

    print("\nInput: Hard coded--no user input.")

    my_tuple1 = ("cherries","apples","bananas","oranges")

    my_tuple2 = 1,2, "three","four"


    print("\nOutput:")
    print("print my_tuple1:")
    print(my_tuple1)

    print()

    print("Print my_tuple2:")
    print(my_tuple2)

    print()

    fruit1, fruit2, fruit3, fruit4 = my_tuple1
    print("Print my_tuple1 unpacking:")
    print(fruit1,fruit2,fruit3,fruit4)

    print()
    print("Print third element in my_tuple2:")
    print(my_tuple2[2])

    print()

    print("Print \"slice\" of my_tuple1 (second and third elements):")
    print(my_tuple1[1:3])

    print()
    print("Reassign my_tuple2 using parentheses.")
    my_tuple2 = (1,2,3,4)
    print("Print my_tuple2:")
    print(my_tuple2)

    print()
    print("Reassign my_tuple2 using \"packing\" method (no parentheses).")
    my_tuple2 = 5,6,7,8

    print("Print my_tuple2:")
    print(my_tuple2)

    print()
    print("Print number of elements in my_tuple1:" +str(len(my_tuple1)))

    print()
    print("Print type my_tuple1:" + str(type(my_tuple1)))

    print()
    print("Delete my_tuple1: \nNote: will generate error, if trying to print after, as it no longer exists.")
    del my_tuple1