def get_requirements():
    print("Temperature Conversion Program")
    print( "Program requirments:\n"
        + "\n1. Program converts user-entered temperatures into Fahrenheit or Celsius scales."
        + "\n2. Program continues to prompt for user entry until no longer requested"
        + "\n3. Note: upper and lower case letteres permitted. THough, incorrect entries are not permitted."
        + "\n4. Note: Program does not validate numeric data (optional requirement).")

def temperature_conversion():
    temperature = 0.0
    choice = ''
    type = ''

    print("Input")

    choice = input("DO you want to convert a temperature (y or n)? ").lower()

    print("\nOutput:")

    while (choice[0] == 'y'):
        type = input(
            "Fahrenheit to Celsius? Type \"f\", or Celsius to Fahrenheit? Type \"C\": ").lower()

        if type[0] == 'f':
            temperature = float(input("Enter temperature in Fahrenheit: "))
            temperature = ((temperature - 32)*5)/9
            print("Temperature in Celsius = " + str(temperature))
            choice = input("\nDo you want to convert another temperature (y or no)? ").lower()

        elif type[0] == 'c':
            temperature = float(input("Enter temperature in Celsius: "))
            temperature = (temperature * 9/5) + 32
            print("Temperature in Fahrenheit = " + str(temperature))
            choice = input("\nDo you want to convert another temperature (y or n)? ").lower()

        else:
            print("Incorrect entry. Please try again.")
            choice = input("\nDo you want to convert a temperature (y or n)? ").lower()

    print("\nThank you for using our Temperature COnversion Program!")
