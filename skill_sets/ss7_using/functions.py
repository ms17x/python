def get_requirements():
    print("Python Lists.\n")
    print( "Program requirments:\n"
        + "1. Lists (Python data structure): mutable, ordered sequence of elements.\n"
        + "2. List are mutable/chaneable--that is, can insert, update, delete.\n"
        + "3. Create lists - using square brackets [list]: my_lists = [cherries","apples", "Banana" , "oranges ].\n"
        + "4. Create a program that mirrors the following IPO (input/process/output) format.\n")

def using_list(): 
   # print("\nInput:")
   # e0 = (input("Enter number of elements: "))
   # e1 = (input("\nPlease enter list element 1: "))
   # e2 = (input("Please enter list element 2: "))
   # e3 = (input("Please enter list element 3: "))
   # e4 = (input("Please enter list element 4: "))
   # lists = [e0,e1,e2,e3,e4]
    start = 0
    end = 0
    lists = []

    print("Input:")
    num = int(input("Enter number of list elements: "))

    print()

    for i in range(num):
        my_element = input('Please enter list element' + str(i + 1) + ":")
        lists.append(my_element)

    print("\nOutput:")
    print("\nPrint my lists:")
    print(lists)
    e5 = (input("\nPlease enter lists elements: "))
    pos = int(input("Please enter lists *index* position (note: must convert to int): "))
    print("\nInsert element into specific position in my_list")
    lists.insert(pos,e5)
    print(lists)
    print("\nCount number of elements in lists")
    print(len(lists))
    print("\nSort elements in list alphabetically: ")
    lists.sort()
    print(lists)
    print("\nReverse lists: ")
    lists.reverse()
    print(lists)
    print("\nRemove last list elembents: ")
    del lists[-1]
    print(lists)
    print('\nDelete second element from list by *index* (note: 1=2nd element):  ')
    lists.pop(1)
    print(lists)
    print("\nDelete element from list by *value* (cherries): ")
    lists.remove("cherries")
    print(lists)
    print("\nDelete all elements from lists: ")
    lists.clear()
    print(lists)
