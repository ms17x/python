def get_requirements():
    print("Python sets - like mathematical sets!")
    print( "Program requirments:\n"
        + "1. Lists (Python data structure): mutable, ordered sequence of elements.\n"
        + "2. List are mutable/chaneable--that is, can insert, update, delete.\n"
        + "3. Create lists - using square brackets [list]: my_lists = [cherries","apples", "Banana" , "oranges ].\n"
        + "4. Create a program that mirrors the following IPO (input/process/output) format.\n"
        + "5. two methods to create sets:\n"
        + "4. Create a program that mirrors the following IPO (input/process/output) format.\n"
        + "4. Create a program that mirrors the following IPO (input/process/output) format.\n")

def using_sets():

    print("\nInput: Hard coded--no user input. see three exampls above")
    print("***NOTE***: \nInput: Hard coded--no user input. see three exampls above")

    my_set = {1,3.14, 2.0, 'four','five'}
    print("print my_set created using curly brackets:")
    print(my_set)

    print("\nPrint type of my_set:")
    print(type(my_set))

    my_set1 = set([1,3.14, 2.0, 'four','five'])
    print("lnprint my_set1 created using set() function with list")
    print(my_set1)

    my_set2 = set((1,3.14, 2.0, 'four','five'))
    print("lnprint my_set2 created using set() function with tuple")
    print(my_set2)

    print("\nprint type of my_set2:")
    print(type(my_set2))

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nDiscar 'four':")
    my_set.discard('four')
    print(my_set)

    print("\nRemove 'five':")
    my_set.remove('five')
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nadd e;ements to set (4) using add() method:")
    my_set.add(4)
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nDisplay minimum number:")
    print(min(my_set))

    print("\nDisplay maximum number:")
    print(max(my_set))

    print("\nDisplay sum of numbers:")
    print(sum(my_set))

    print("\nDisplay maximum number:")
    print(min(my_set))

    print("\nDelete all set elements:")
    my_set.clear()
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))





    





    









    