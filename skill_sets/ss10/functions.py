def get_requirements():
    print("Python sets - like mathematical sets!")
    print( "Program requirments:\n"
        + "\n1. dictionaries (Python data structure): unorderded key: value pairs."
        + "\n2. dictionaries: an associative array (also known as hashes)."
        + "\n3. any key in a dictionary is assiciated (or mapped) to a value (i.e., any python dat type)."
        + "\n4. keys: must be of imutable type (string, number or tuple with imutable elements)."
        + "\n5. values: can be any data type and can repeat."
        + "\n6. create a program that mirros the following IPO (input/process/output) format."
        + "\n    Create empty dictionary, using curly braces {}: my_dictionary = {}"
        + "\n    use the following keys: fname, lname, degree, major. gpa"
        + "\n    Note: dictionaries have key-values pairs instead of single values; this differentiates a dictionary from a set.")

def dictionaries():

    v_fname = ""
    v_lname = ""
    v_degree = ""
    v_major = ""
    v_gpa = 0.0
    my_dictionary = {}

    print("Input:")
    v_fname = input("First name: ")
    v_lname = input("Last Name: ")
    v_degree = input("Degree: ")
    v_major = input("Major (IT or ICT): ")
    v_gpa = float(input("Gpa: "))

    print()

    my_dictionary['fname'] = v_fname
    my_dictionary['lname'] = v_lname
    my_dictionary['degree'] = v_degree
    my_dictionary['major'] = v_major
    my_dictionary['gpa'] = v_gpa

    print("\nOutput:")
    print("Print my_dictionary:")
    print(my_dictionary)

    print("\nReturn view of dictionary's (key, value) pair, built-in function:")
    print(my_dictionary.items())

    print("\nReturn view object of all values in dictionary, built-in function:")
    print(my_dictionary.keys())

    print("\nReturn view object of all values in dictionary, built-in function:")
    print(my_dictionary.values())

    print("\nPrint only first and last names, using keys:")
    print(my_dictionary['fname'], my_dictionary['lname'])

    print("\nPrint only first and last names, using get() function:")
    print(my_dictionary.get("fname"), my_dictionary.get("lname"))

    print("\nCount number of items (key:value pairs) in dictionary: ")
    print(len(my_dictionary))

    print("\nRemove last dictionary item (popitem): ")
    my_dictionary.popitem()
    print(my_dictionary)

    print("\nDelete major from dictionary, using key: ")
    my_dictionary.pop("major")
    print(my_dictionary)

    print("\nReturn object type: ")
    print(type(my_dictionary))

    print("\nDelete all items from list: ")
    my_dictionary.clear()
    print(my_dictionary)

    del my_dictionary


